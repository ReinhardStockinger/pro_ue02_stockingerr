package OPR.UE07.SortableList;

public interface Sortable {
	// insert one element
	public void insertSorted(Comparable _value, boolean _ascending);

	// create a new Sortable which is sorted ascending
	public Sortable sortAscending();

	// create a new Sortable which is sorted descending
	public Sortable sortDescending();
}
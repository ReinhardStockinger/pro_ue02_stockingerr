package OPR.UE07.SortableList;

public class DoubleLinkedList {

	// Pointer to the first and last element of the list
	private DLNode head, tail;

	// Constructor initializes list with a standard size.
	public DoubleLinkedList() {
		head = null;
		tail = null;
	}

	/*
	 * Copy constructor initializes list with another list. This constructor
	 * must COPY all elements of the other list. The elements of the other list
	 * must NOT be changed!
	 */
	public DoubleLinkedList(DoubleLinkedList other) {
		if (other != null) {
			DLNode p = other.head;
			while (p != null) {
				pushBack(p.GetValue());
				p = p.GetNext();
			}
		}
	}

	// Deinitializes the object; think about it and comment what to do here.
	protected void finalize() {
		head = null;
		tail = null;
	}

	// Adds all elements from another list at the front of this linked list.
	public void pushFront(DoubleLinkedList other) {
		DLNode p = head;
		if (other.head != null || other.elements() != 0) {
			other.tail.SetNext(p);
			p.SetPrev(other.tail);
			head = other.head;
		}

	}

	// Adds all elements from another list at the back of this linked list.
	public void pushBack(DoubleLinkedList other) {
		if (other.head != null || other.elements() != 0) {
			DLNode p = other.head;
			tail.SetNext(p);
			p.SetPrev(tail);
			tail = other.tail;
		}
	}

	// Clones this DoubleLinkedList instance and returns an exact COPY.
	public DoubleLinkedList clone() {
		return new DoubleLinkedList(this);
	}

	// Returns true if the other list is equal to this one, false otherwise. The
	// contents of the two lists must not be changed!
	public boolean equals(DoubleLinkedList other) {
		DLNode p = head, q = other.head;
		if (p == null && q == null) {
			return true;
		}
		if (elements() != other.elements()) {
			return false;
		}
		while (p != null && q != null) {
			if (p.GetValue() != q.GetValue()) {
				return false;
			}
			p = p.GetNext();
			q = q.GetNext();
		}
		return true;
	}

	// Returns a string representation of the list. See the exercise
	// specification below for the exact string format!
	public String toString() {
		DLNode p = head;
		StringBuffer s = new StringBuffer();
		s.append("List: [");
		while (p != null) {
			if (p.GetNext() == null) {
				s.append(p.GetValue());
				p = p.GetNext();
			} else {
				s.append(p.GetValue() + ",");
				p = p.GetNext();
			}
		}
		s.append(".]");
		return s.toString();
	}

	// Returns true if the element val exists in the list, false otherwise.
	public boolean search(Comparable val) {
		DLNode p = head;
		while (p != null && p.GetValue() != val) {
			p = p.GetNext();
		}
		if (p != null && p.GetValue() == val) {
			return true;
		} else {
			return false;
		}
	}

	// Methods of Exercise 1

	// Clears all elements from the linked list
	public void clear() {
		head = null;
		tail = null;
	}

	// Adds an element at the front of the linked list.
	public void pushFront(Comparable val) {
		DLNode p = head;
		if (head == null) {
			head = tail = new DLNode(val, null, null);
		} else {
			DLNode help = new DLNode(val);
			help.SetNext(p);
			help.SetPrev(null);
			head = help;
			p.SetPrev(help);
		}
	}

	// Adds an element at the back of the linked list.
	public void pushBack(Comparable val) {
		DLNode p = head;
		if (head == null) {
			head = tail = new DLNode(val, null, null);
		} else {
			p = new DLNode(val);
			tail.SetNext(p);
			p.SetPrev(tail);
			tail = p;
		}
	}

	/**
	 * Removes and returns the front element of the linked list. Throws a
	 * InvalidAccessException if the list is empty.
	 */
	public Comparable popFront(){
		DLNode p = head;
		if (head == null) {
			return null;
		} else {
			Comparable value = p.GetValue();
			if (head == tail) {
				head = null;
				tail = head;
			} else {
				head = p.GetNext();
				head.SetPrev(null);
			}
			return value;
		}
	}

	// Returns the front element of the list without removing it. Returns
	// Integer.MIN_VALUE if empty
	public Comparable peekFront() {
		DLNode p = head;
		if (head == null) {
			return Integer.MIN_VALUE;
		} else {
			return p.GetValue();
		}
	}

	/**
	 * Removes and returns the element from the back of the linked list. Throws
	 * a InvalidAccessException if the list is empty.
	 */
	public Comparable popBack(){
		if (head == null) {
			return null;
		} else {
			Comparable value = tail.GetValue();
			if (head == tail) {
				head = null;
				tail = head;
			} else {
				tail.GetPrev().SetNext(null);
				tail = tail.GetPrev();
			}
			return value;
		}
	}

	// Returns the element at the back of the list without removing it. Returns
	// Integer.MIN_VALUE if empty
	public Comparable peekBack() {
		if (head == null) {
			return null;
		} else {
			return tail.GetValue();
		}
	}

	// Returns the number of elements in the double linked list
	public int elements() {
		DLNode p = head;
		int intCounter = 0;
		while (p != null) {
			intCounter++;
			p = p.GetNext();
		}
		return intCounter;
	}

	// Reverses the order of all elements in the list. �He who is first, shall
	// be last!�
	public void reverse() {
		DLNode help = head;
		head = tail;
		tail = help;
		DLNode p = head;

		while (p != null) {
			help = p.GetNext();
			p.SetNext(p.GetPrev());
			p.SetPrev(help);
			p = p.GetNext();
		}
	}

	// Getter method for head.
	public DLNode getHead() {
		return head;
	}

	// Getter method for tail.
	public DLNode getTail() {
		return tail;
	}

	// Setter method for head.
	public void setHead(DLNode head) {
		this.head = head;
	}

	// Setter method for tail.
	public void setTail(DLNode tail) {
		this.tail = tail;
	}
}

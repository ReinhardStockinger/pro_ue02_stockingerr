package OPR.UE07.SortableList;

public class RandomAccessDoubleLinkedList extends DoubleLinkedList {
	/** Initializes an empty list. */
	public RandomAccessDoubleLinkedList() {
		super();
	}

	/**
	 * Copy constructor which initializes the list with another list. This
	 * constructor must COPY all elements of the other list.
	 */
	public RandomAccessDoubleLinkedList(RandomAccessDoubleLinkedList other) {
		super(other);
	}

	/**
	 * Inserts a new element with value val at the given index. If the index is
	 * larger than the current size, the list is padded with uninitialized DLNod
	 * es . If the node, which was at the given index before the insertion, is
	 * not initialized, then that Node must be reused. Should index be < 0, then
	 * do nothing.
	 */
	public void insertAt(int index, Comparable val) {
		
		DLNode dlNew = new DLNode(val);
		int intIndexCounter = 0;
		
		if (index < this.elements()) {
			DLNode p = getHead();
			
			while (p != null && intIndexCounter != index) {
				p = p.GetNext();
				intIndexCounter++;
			}
			if (p == getHead() && index == 0) {
				getHead().SetPrev(dlNew);
				dlNew.SetNext(getHead());
				setHead(dlNew);
			} else if (p == getTail() && elements() == index) {
				getTail().SetNext(dlNew);
				dlNew.SetPrev(getTail());
				setTail(dlNew);

			} else if (p != null && intIndexCounter == index) {

				dlNew.SetNext(p);
				p.GetPrev().SetNext(dlNew);
				dlNew.SetPrev(p.GetPrev());
				p.SetPrev(dlNew);

			} else {
				setHead(dlNew);
			}
		}else{
			int intFillUp = this.elements();
			while(intFillUp != index){
				this.pushBack(Integer.MIN_VALUE);
				intFillUp++;
			}
			this.pushBack(val);
		}
	}

	/**
	 * Returns true if an element with the given value exists, false otherwise.
	 * However, true is returned upon the first occurrence of val.
	 */
	public boolean contains(Comparable val) {
		return super.search(val);
	}

	/**
	 * Removes the element at the given index. False if returned if index >
	 * list�s size.
	 */
	public boolean removeAt(int index) {
		DLNode p = getHead();
		int intIndexCounter = 0;
		while (p != null && intIndexCounter != index) {
			p = p.GetNext();
			intIndexCounter++;
		}
		if (p == getHead() && p != null) {
			if (p.GetNext() != null) {
				setHead(p.GetNext());
				p.GetNext().SetPrev(null);
			} else {
				setHead(null);
			}
			return true;
		} else if (p == getTail() && p != null) {
			setTail(getTail().GetPrev());
			getTail().SetNext(null);
			return true;
		} else if (p != null && intIndexCounter == index) {
			p.GetPrev().SetNext(p.GetNext());
			p.GetNext().SetPrev(p.GetPrev());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Removes all elements with the given value. False if returned if val was
	 * not found .
	 */
	public boolean removeAll(Comparable val) {
		DLNode p = getHead();
		int intIndexCounter = 0;
		boolean boolRemoved = false;
		while (p != null) {
			if (p.GetValue() == val) {
				removeAt(intIndexCounter);
				boolRemoved = true;
				p = p.GetNext();
			} else {
				intIndexCounter++;
				p = p.GetNext();
			}
		}
		return boolRemoved;
	}

	/**
	 * Returns the integer value at the given index. If index > list�s size,
	 * Integer.MIN_VALUE is returned.
	 */
	public Comparable elementAt(int index) {
		DLNode p = getHead();
		int intIndexCounter = 0;
		while (p != null && intIndexCounter != index) {
			p = p.GetNext();
			intIndexCounter++;
		}
		if (p != null && intIndexCounter == index) {
			return p.GetValue();
		} else {
			return Integer.MIN_VALUE;
		}
	}
}

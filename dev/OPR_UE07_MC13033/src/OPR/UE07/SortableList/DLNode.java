package OPR.UE07.SortableList;

public class DLNode {

	// Ref to the next elem in the list, or null if it is the last
	private DLNode next;
	// Ref to the prev elem in the list, or null if it is the first
	private DLNode prev;
	// Holds the actual data
	private Comparable value;
	// Holds another list. Must be null if val != Integer.MIN_VALUE
	private DoubleLinkedList list;

	// Standard-Constructor initializes Node.
	public DLNode() {
		this(0, null, null);
		//list = null;
	}

	// Constructor initializes Node with parameter value.
	public DLNode(Comparable value) {
		this(value, null, null);
	}

	// Constructor initializes Node with parameter value, next, prev.
	public DLNode(Comparable value, DLNode next, DLNode prev) {
		this.value = value;
		this.next = next;
		this.prev = prev;
	}
	
	// Returns true if the list is initialized, otherwise false.
	public boolean isInitialized() {
		return list != null;
	}

	// Returns value
	public Comparable GetValue() {
		return value;
	}

	// Set value
	public void SetValue(Comparable value) {
		this.value = value;
	}

	// Returns next
	public DLNode GetNext() {
		return next;
	}

	// Set next
	public void SetNext(DLNode next) {
		this.next = next;
	}

	// Returns prev
	public DLNode GetPrev() {
		return prev;
	}

	// Set prev
	public void SetPrev(DLNode prev) {
		this.prev = prev;
	}

	// Returns list
	public DoubleLinkedList GetDLList() {
		return list;
	}

	// Set list
	public void SetDLList(DoubleLinkedList list) {
		this.list = list;
	}
}

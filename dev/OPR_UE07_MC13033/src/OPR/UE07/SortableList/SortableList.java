package OPR.UE07.SortableList;

public class SortableList extends RandomAccessDoubleLinkedList implements
		Sortable {

	@Override
	public void insertSorted(Comparable _value, boolean _ascending) {
		DLNode h = getHead();
		for (int i = 0; i < elements(); i++) {
			if (_ascending == true &&_value.compareTo(h.GetValue()) < 0) {
				insertAt(i, _value);
				return;
			}else if(_ascending == false &&_value.compareTo(h.GetValue()) > 0){
				insertAt(i, _value);
				return;
			}
			h = h.GetNext();
		}
		pushBack(_value);

	}

	@Override
	public Sortable sortAscending() {
		
		Sortable sortedList = new SortableList();
		DLNode h = getHead();
		for(int i = 0; i < elements(); i++){
			sortedList.insertSorted(h.GetValue(), true);
			h = h.GetNext();
		}
		
		return sortedList;
	}

	@Override
	public Sortable sortDescending() {
		Sortable sortedList = new SortableList();
		DLNode h = getHead();
		for(int i = 0; i < elements(); i++){
			sortedList.insertSorted(h.GetValue(), false);
			h = h.GetNext();
		}
		return sortedList;
	}

}

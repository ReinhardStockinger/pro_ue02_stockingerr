

public class Car implements Comparable<Car> {
	private double length;
	// relevant for comparison
	private double width;
	// not relevant for comp.
	private double engineDisplacement;
	// not relevant for comp.
	
	// Constructor
	public Car(double length){
		this.length = length;
	}
	
	// Default-Constructor
	public Car(){
		this.length = 3.5;
	}
	
	/**
	 * Compares two car objects.
	 * Returns: 1 -> passed carlength is smaller
	 * Returns:	0 -> passed carlength is similar
	 * Returns:-1 -> passed carlength is bigger
	 */
	public int compareTo(Car other) {
		if (this.length > other.length) {
			return 1;
		} else if (this.length == other.length)
			return 0;
		else
			return -1;
	}
	
}

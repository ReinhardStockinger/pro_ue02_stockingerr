import static org.junit.Assert.*;

import org.junit.Test;

import OPR.UE07.SortableList.RandomAccessDoubleLinkedList;

public class RandomAccessDoubleLinkedListJUnit {

	RandomAccessDoubleLinkedList radll = new RandomAccessDoubleLinkedList();

	@Test
	public void testElementAtCorrect(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(c3, radll.elementAt(2));
	}

	@Test
	public void testContainsTrue() {
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(true, radll.contains(c1));
		assertEquals(true, radll.contains(c2));
		assertEquals(true, radll.contains(c3));
	}

	@Test
	public void testContainsFalse() {
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		Car c0 = new Car(0);
		Car c4 = new Car(4);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(false, radll.contains(c0));
		assertEquals(false, radll.contains(c4));
	}

	@Test
	public void testContainsEmptyList() {
		Car c0 = new Car(0);
		Car c1 = new Car(1);
		assertEquals(false, radll.contains(c0));
		assertEquals(false, radll.contains(c1));
	}

	@Test
	public void testRemovesAtEndIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(true, radll.removeAt(2));
		assertEquals(c2, radll.popBack());
		assertEquals(c1, radll.popBack());
	}

	@Test
	public void testRemovesAtMiddleIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(true, radll.removeAt(1));
		assertEquals(c3, radll.popBack());
		assertEquals(c1, radll.popBack());
	}

	@Test
	public void testRemovesAtStartIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		assertEquals(true, radll.removeAt(0));
		assertEquals(c3, radll.popBack());
		assertEquals(c2, radll.popBack());
	}

	@Test
	public void testRemovesAtOnlyOneItemInList(){
		Car c1 = new Car(1);
		radll.pushBack(c1);
		assertEquals(true, radll.removeAt(0));
		assertEquals(0, radll.elements());
	}

	@Test
	public void testRemovesAtEmptyList(){
		assertEquals(false, radll.removeAt(0));
	}

	@Test
	public void testRemovesAll(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		Car c4 = new Car(4);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c1);
		radll.pushBack(c3);
		radll.pushBack(c1);
		radll.pushBack(c4);
		radll.pushBack(c1);
		assertEquals(true, radll.removeAll(c1));
		assertEquals(c4, radll.popBack());
		assertEquals(c3, radll.popBack());
		assertEquals(c2, radll.popBack());
	}

	@Test
	public void testRemovesAllEmptyList(){
		assertEquals(false, radll.removeAll(2));
	}

	@Test
	public void testRemovesAllNoValueRemoved(){
		Car c1 = new Car(2);
		Car c2 = new Car(1);
		Car c3 = new Car(2);
		Car c4 = new Car(3);
		Car c5 = new Car(2);
		Car c6 = new Car(5);
		Car c7 = new Car(2);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.pushBack(c3);
		radll.pushBack(c4);
		radll.pushBack(c5);
		radll.pushBack(c6);
		radll.pushBack(c7);
		assertEquals(false, radll.removeAll(4));
	}

	@Test
	public void testInsertAtStartIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c2);
		radll.pushBack(c3);
		radll.insertAt(0, c1);
		assertEquals(c3, radll.popBack());
		assertEquals(c2, radll.popBack());
		assertEquals(c1, radll.popBack());
	}

	@Test
	public void testInsertAtMiddleIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c3);
		radll.insertAt(1, c2);
		assertEquals(c3, radll.popBack());
		assertEquals(c2, radll.popBack());
		assertEquals(c1, radll.popBack());
	}

	@Test
	public void testInsertAtEndIndex(){
		Car c1 = new Car(1);
		Car c2 = new Car(2);
		Car c3 = new Car(3);
		radll.pushBack(c1);
		radll.pushBack(c2);
		radll.insertAt(2, c3);
		assertEquals(c3, radll.popBack());
		assertEquals(c2, radll.popBack());
		assertEquals(c1, radll.popBack());
	}

	@Test
	public void testInsertAtIndexHigherThanCurrentSize(){
		Car c1 = new Car(1);
		Car c5 = new Car(5);
		radll.pushBack(c1);
		radll.insertAt(5, c5);
		assertEquals(c5, radll.popBack());
		assertEquals(Integer.MIN_VALUE, radll.popBack());
		assertEquals(Integer.MIN_VALUE, radll.popBack());
		assertEquals(Integer.MIN_VALUE, radll.popBack());
		assertEquals(Integer.MIN_VALUE, radll.popBack());
		assertEquals(c1, radll.popBack());
	}
}

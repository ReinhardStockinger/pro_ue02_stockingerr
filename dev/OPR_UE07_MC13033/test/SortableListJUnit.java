import static org.junit.Assert.*;

import org.junit.Test;

import OPR.UE07.SortableList.Sortable;
import OPR.UE07.SortableList.SortableList;

public class SortableListJUnit {

	SortableList sl = new SortableList();

	@Test
	public void test(){
		sl.pushBack(88);
		sl.pushBack(5);
		sl.insertSorted(100, true);
		Sortable sl2 = sl.sortDescending();
		System.out.println(sl2.toString());
	}
}
